create table addresses (
  id        bigserial primary key,
  line1     text not null,
  line2     text,
  line3     text,
  line4     text not null,
  post_code text not null,
  city      text not null,
  country   text not null
);

create table properties (
  id                  bigserial primary key,
  owner               text   not null,
  address_id          bigint not null,
  number_of_bedrooms  integer,
  number_of_bathrooms integer,
  income_generated    numeric(19, 4),
  airbnb_id           text   not null,

  foreign key (address_id) references addresses (id)
);

create table snapshots (
  id          bigserial primary key,
  property_id bigint    not null references properties (id),
  created_on  timestamp not null default now()
);

create table diffs (
  snapshot_id bigint not null,
  action      text   not null,
  path        text   not null,
  old_value   text,
  new_value   text,

  foreign key (snapshot_id) references snapshots (id),
  unique (snapshot_id, path)
);
create index on diffs (snapshot_id);

create table airbnb_validation_requests (
  id         bigserial primary key,
  created_on timestamp not null default now(),
  airbnb_id  text      not null
);

create table airbnb_validation_results (
  request_id bigint    not null references airbnb_validation_requests (id),
  result     text      not null,
  created_on timestamp not null default now()
);

create view last_airbnb_validation_results as
  with last_res as (
      select request_id, max(created_on) as created_on
      from airbnb_validation_results
      where result is not null
      group by request_id
  )

  select r.*
  from airbnb_validation_results r
    left join last_res lr on lr.request_id = r.request_id and lr.created_on = r.created_on;
