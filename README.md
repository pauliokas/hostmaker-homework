# Hostmakers Homework

Backend homework for hostmakers.

## Running the app

To start the app just run `docker-compose up`.

You can run tests with `npm run test`.

## API

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/e21a529bc1a5fb690ead)

If you don't have Postman, then check-out tests in `./lib/__test__`.
