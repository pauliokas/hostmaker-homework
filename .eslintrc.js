module.exports = {
    extends: "airbnb-base",
    overrides: [
        {
            files: [ "**/__test__/**/*.test.js" ],
            env: {
                jest: true
            }
        }
    ]
};
