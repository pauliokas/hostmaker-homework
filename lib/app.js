const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');

const config = require('./config');
const logger = require('./common/logging');
const { ApplicationError } = require('./domain/common/errors');

const validationJob = require('./scheduled/airbnb-id-validation.job');

const app = new Koa();

app.on('error', (err) => {
  if (err instanceof ApplicationError) {
    return;
  }

  logger.error(`Unexpected error. ${err.stack}`);
});

const errorHandler = async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    if (err.body) {
      ctx.type = 'json';
      ctx.body = err.body;
    }

    ctx.app.emit('error', err, ctx);
  }
};

const router = new Router()
  .use('/changes', require('./api/changes'))
  .use('/properties', require('./api/properties'));

app.use(errorHandler);
app.use(bodyParser());

app.use(router.routes());

module.exports = {
  start: () => {
    app.listen(config.port);
    logger.info(`App listening on port ${config.port}`);

    validationJob.start(10000);
  },
};
