const Router = require('koa-router');
const controller = require('./properties.ctrl');

module.exports = new Router()
  .get('/', controller.getAll)
  .get('/:id', controller.get)
  .post('/', controller.create)
  .put('/:id', controller.update)
  .patch('/:id', controller.patch)
  .routes();
