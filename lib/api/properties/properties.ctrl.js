const Properties = require('../../domain/properties/properties.srvc');

const create = async (ctx) => {
  const created = await Properties.create(ctx.request.body);

  ctx.response.status = 201;
  ctx.response.set('Location', `${ctx.request.href}/${created.id}`);
  ctx.body = created;
};

const update = async (ctx) => {
  const updated = await Properties.update(ctx.params.id, ctx.request.body);

  ctx.response.status = 200;
  ctx.body = updated;
};

const get = async (ctx) => {
  const fetchedProperty = await Properties.get(ctx.params.id);

  ctx.response.status = 200;
  ctx.body = fetchedProperty;
};

const getAll = async (ctx) => {
  const fetchedProperties = await Properties.getAll();

  ctx.response.status = 200;
  ctx.body = fetchedProperties;
};

const patch = async (ctx) => {
  const updatedProperty = await Properties.patch(ctx.params.id, ctx.request.body);

  ctx.response.status = 200;
  ctx.body = updatedProperty;
};

module.exports = {
  create,
  update,
  get,
  getAll,
  patch,
};
