const Router = require('koa-router');
const controller = require('./changes.ctrl');

module.exports = new Router()
  .get('/', controller.fetch)
  .routes();
