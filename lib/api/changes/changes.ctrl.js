const { fetchChanges } = require('../../domain/versioning/versioning.srvc');
const { isArray } = require('lodash');

const convertParams = (param, mapper = p => p) => {
  let converted = param;
  if (typeof converted === 'string') {
    if (!converted) {
      return [];
    }

    converted = [converted];
  }

  if (isArray(converted)) {
    return converted.map(mapper);
  }

  return converted;
};

const fetch = async (ctx) => {
  ctx.body = await fetchChanges({
    after: ctx.query.after,
    before: ctx.query.before,
    actions: convertParams(ctx.query['actions[]']),
    paths: convertParams(ctx.query['paths[]']),
    propertyIds: convertParams(ctx.query['propertyIds[]'], change => Number.parseInt(change, 10)),
  });
  ctx.status = 200;
};

module.exports = {
  fetch,
};
