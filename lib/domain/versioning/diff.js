const _ = require('lodash');

const changed = (oldValue, newValue) => ({
  action: 'CHANGED', oldValue, newValue, path: [],
});

const added = (oldValue, newValue, path) => ({
  action: 'ADDED', oldValue, newValue, path: path || [],
});

const appendPath = path => change => ({ ...change, path: [path, ...change.path] });

const addedRecursive = (keys, obj) =>
  keys
    .map((key) => {
      const inner = obj[key];
      if (!_.isObject(inner)) {
        return [added(undefined, inner, [key])];
      }

      return addedRecursive(Object.keys(inner), inner).map(appendPath(key));
    })
    .reduce((result, arr) => result.concat(arr), []);

// TODO: support removed values
// TODO: support arrays
const diff = (left, right) => {
  if (_.isEqual(left, right)) {
    return [];
  }

  if (!_.isObject(left) && !_.isObject(right)) {
    return [changed(left, right)];
  }

  const leftKeys = Object.keys(left || {});
  const rightKeys = Object.keys(right || {});

  const addedKeys = rightKeys.filter(key => !leftKeys.includes(key));
  const changedKeys = leftKeys.filter(key => rightKeys.includes(key));

  const changes = [];

  changedKeys
    .map((key) => {
      const innerDiff = diff(left[key], right[key]);
      return innerDiff.map(appendPath(key));
    })
    .forEach(ch => changes.push(...ch));

  changes.push(...addedRecursive(addedKeys, right));

  return changes;
};

module.exports = diff;
