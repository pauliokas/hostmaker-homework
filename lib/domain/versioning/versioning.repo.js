const saveDiff = async (client, snapshotId, diff) => {
  const query = {
    name: 'create-diff',
    // language=SQL
    text: `
      insert into diffs (snapshot_id, action, path, old_value, new_value) values
        ($1, $2, $3, $4 :: text, $5 :: text)
    `,
  };

  await Promise.all(diff.map(change =>
    client.query({
      ...query,
      values: [
        snapshotId,
        change.action,
        change.path.join('.'),
        change.oldValue,
        change.newValue,
      ],
    })));
};

const saveSnapshot = async (client, propertyId, diff) => {
  const query = {
    name: 'create-snapshot',
    // language=SQL
    text: `
      insert into snapshots (property_id) values
        ($1)
      returning id
    `,
  };

  const snapshotResult = await client.query({
    ...query,
    values: [propertyId],
  });

  await saveDiff(client, snapshotResult.rows[0].id, diff);
};

const createSnapshot = (createdOn, propertyId) => ({
  createdOn,
  propertyId,
  diffs: [],
});

const fetchChanges = async (client, filter) => {
  const query = {
    name: 'fetch-diff',
    // language=SQL
    text: `
      select
        s.id,
        s.property_id,
        s.created_on,
        d.snapshot_id,
        d.action,
        d.path,
        d.old_value,
        d.new_value
      from snapshots s
        join diffs d on s.id = d.snapshot_id
      where (nullif($1, '') is null or s.created_on >= $1::timestamp)
            and (nullif($2, '') is null or s.created_on <= $2::timestamp)
            and ($3::text[] is null or d.action = any ($3::text[]))
            and ($4::text[] is null or d.path = any ($4::text[]))
            and ($5::bigint[] is null or s.property_id = any ($5::bigint[]))
      order by s.created_on, s.id, d.path
    `,
  };

  const result = await client.query({
    ...query,
    values: [
      filter.after,
      filter.before,
      filter.actions,
      filter.paths,
      filter.propertyIds,
    ],
  });

  const snapshots = {};
  const mapped = [];
  result.rows.forEach((row) => {
    let snapshot = snapshots[row.id];
    if (!snapshot) {
      snapshot = createSnapshot(row.created_on, row.property_id);
      snapshots[row.id] = snapshot;
      mapped.push(snapshot);
    }

    snapshot.diffs.push({
      action: row.action,
      path: row.path,
      oldValue: row.old_value,
      newValue: row.new_value,
    });
  });

  return mapped;
};

module.exports = {
  saveSnapshot,
  fetchChanges,
};
