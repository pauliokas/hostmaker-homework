const diff = require('../diff.js');

const change = (action, oldValue, newValue, path = []) => [
  {
    action,
    oldValue,
    newValue,
    path,
  },
];

describe('diff', () => {
  it('handles simple types', () => {
    expect(diff(null, 1)).toEqual(change('CHANGED', null, 1));
    expect(diff(undefined, 1)).toEqual(change('CHANGED', undefined, 1));
    expect(diff(1, 1)).toEqual([]);
    expect(diff(1, 2)).toEqual(change('CHANGED', 1, 2));
    expect(diff('s1', 's2')).toEqual(change('CHANGED', 's1', 's2'));
    expect(diff(1, Number(1))).toEqual([]);
  });

  it('handles simple objects', () => {
    expect(diff(null, { a: 1 })).toEqual(change('ADDED', undefined, 1, ['a']));
    expect(diff(undefined, { a: 1 })).toEqual(change('ADDED', undefined, 1, ['a']));
    expect(diff({}, { a: 1 })).toEqual(change('ADDED', undefined, 1, ['a']));
    expect(diff({ a: 1 }, { a: 1 })).toEqual([]);
    expect(diff({ a: 1 }, { a: 2 })).toEqual(change('CHANGED', 1, 2, ['a']));
    expect(diff({ a: 1, b: 1 }, { a: 2, b: 1 })).toEqual(change('CHANGED', 1, 2, ['a']));
  });

  it('handles nested objects', () => {
    expect(diff(null, { a: { b: 1 } })).toEqual(change('ADDED', undefined, 1, ['a', 'b']));
    expect(diff(undefined, { a: { b: 1 } })).toEqual(change('ADDED', undefined, 1, ['a', 'b']));
    expect(diff({}, { a: { b: 1 } })).toEqual(change('ADDED', undefined, 1, ['a', 'b']));
    expect(diff({ a: { b: 1 } }, { a: { b: 1 } })).toEqual([]);
    expect(diff({ a: { b: 1 } }, { a: { b: 2 } })).toEqual(change('CHANGED', 1, 2, ['a', 'b']));
    expect(diff({ a: { b: 1, c: 2 }, d: 3 }, { a: { b: 2, c: 2 }, d: 3 })).toEqual(change('CHANGED', 1, 2, ['a', 'b']));
  });
});
