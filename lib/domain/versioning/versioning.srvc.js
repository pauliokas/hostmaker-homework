const diff = require('./diff');
const Repo = require('./versioning.repo');
const executeInTransaction = require('../common/pool');

const saveVersion = async (client, propertyId, oldVersion, newVersion) => {
  const changes = diff(oldVersion, newVersion).filter(change => change.path.length > 1 || change.path[0] !== 'id');

  if (!changes.length) {
    return;
  }

  await Repo.saveSnapshot(client, propertyId, changes);
};

const fetchChanges = filter => executeInTransaction(client => Repo.fetchChanges(client, filter));

module.exports = {
  saveVersion,
  fetchChanges,
};
