const Joi = require('joi');
const executeInTransaction = require('../common/pool');
const { ValidationError, NotFoundError } = require('../common/errors');
const Repo = require('./properties.repo');
const Versioning = require('../versioning/versioning.srvc');
const Validation = require('./validation/airbnb');
const { merge } = require('lodash');

const propertiesSchema = Joi.object()
  .keys({
    id: Joi.number().integer().optional(),
    owner: Joi.string().required(),
    address: Joi.object().keys({
      line1: Joi.string().required(),
      line2: Joi.string().optional(),
      line3: Joi.string().optional(),
      line4: Joi.string().required(),
      postCode: Joi.string().required(),
      city: Joi.string().required(),
      country: Joi.string().required(),
    }).required(),
    numberOfBedrooms: Joi.number().integer().min(0).required(),
    numberOfBathrooms: Joi.number().integer().positive().required(),
    incomeGenerated: Joi.number().positive().required(),
    airbnbId: Joi.string().required(),
  })
  .required();

const enhanceWithAirbnbStatus = async (client, properties) => {
  const airbnbIds = properties.map(p => p.airbnbId);
  const validationResult = await Validation.resolveStatuses(client, airbnbIds);
  return properties.map(p => ({ ...p, airbnbIdValidationStatus: validationResult[p.airbnbId] }));
};

const getEntity = async (client, id) => {
  const property = await Repo.fetchProperty(client, id);
  if (!property) {
    throw new NotFoundError('property', id);
  }

  return property;
};

const get = async id => executeInTransaction(async (client) => {
  const property = await getEntity(client, id);
  return (await enhanceWithAirbnbStatus(client, [property]))[0];
});

const validateProperty = async (property) => {
  const { error } = Joi.validate(property, propertiesSchema, { abortEarly: false });
  if (error && error.details && error.details.length) {
    throw new ValidationError(
      'Invalid property',
      error.details.map(err => ({
        message: err.message,
        path: err.path.join('.'),
      })),
    );
  }
};

const create = async (prp) => {
  await validateProperty(prp);

  return executeInTransaction(async (client) => {
    const property = await Repo.createProperty(client, prp);

    await Promise.all([
      Versioning.saveVersion(client, property.id, null, property),
      Validation.requestIdValidation(client, property.airbnbId),
    ]);

    return (await enhanceWithAirbnbStatus(client, [property]))[0];
  });
};

const updateProperty = async (client, id, property) => {
  await validateProperty(property);

  const oldProperty = await getEntity(client, id);
  const updatedProperty = await Repo.updateProperty(client, id, property);

  const postActions = [Versioning.saveVersion(client, id, oldProperty, updatedProperty)];
  if (oldProperty.airbnbId !== updatedProperty.airbnbId) {
    postActions.push(Validation.requestIdValidation(client, updatedProperty.airbnbId));
  }

  await Promise.all(postActions);

  return (await enhanceWithAirbnbStatus(client, [updatedProperty]))[0];
};

const update = async (id, property) =>
  executeInTransaction(client => updateProperty(client, id, property));

const patch = async (id, propertyPatch) => executeInTransaction(async (client) => {
  const property = await getEntity(client, id);
  merge(property, propertyPatch);
  return updateProperty(client, id, property);
});

const getAll = () => executeInTransaction(async (client) => {
  const properties = await Repo.fetchProperties(client);
  return enhanceWithAirbnbStatus(client, properties);
});

module.exports = {
  create,
  update,
  get,
  getAll,
  patch,
};
