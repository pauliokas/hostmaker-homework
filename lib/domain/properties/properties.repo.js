const mapProperties = (propertiesRow, addressRow = propertiesRow) => ({
  id: Number.parseInt(propertiesRow.id, 10),
  owner: propertiesRow.owner,
  numberOfBedrooms: propertiesRow.number_of_bedrooms,
  numberOfBathrooms: propertiesRow.number_of_bathrooms,
  incomeGenerated: propertiesRow.income_generated,
  airbnbId: propertiesRow.airbnb_id,
  address: {
    line1: addressRow.line1,
    line2: addressRow.line2,
    line3: addressRow.line3,
    line4: addressRow.line4,
    postCode: addressRow.post_code,
    city: addressRow.city,
    country: addressRow.country,
  },
});

const createAddress = async (client, address) => {
  const query = {
    name: 'create-address',
    // language=SQL
    text: `
          insert into addresses (line1, line2, line3, line4, post_code, city, country)
          values ($1, $2, $3, $4, $5, $6, $7)
          returning *
        `,
  };

  const result = await client.query({
    ...query,
    values: [
      address.line1,
      address.line2,
      address.line3,
      address.line4,
      address.postCode,
      address.city,
      address.country,
    ],
  });

  return result.rows[0];
};

const createProperty = async (client, property) => {
  const query = {
    name: 'create-property',
    // language=SQL
    text: `
          insert into properties (address_id, owner, number_of_bedrooms, number_of_bathrooms, income_generated, airbnb_id)
          values ($1, $2, $3, $4, $5, $6)
          returning *
        `,
  };

  const addressRow = await createAddress(client, property.address);

  const result = await client.query({
    ...query,
    values: [
      addressRow.id,
      property.owner,
      property.numberOfBedrooms,
      property.numberOfBathrooms,
      property.incomeGenerated,
      property.airbnbId,
    ],
  });

  return mapProperties(result.rows[0], addressRow);
};

const updateAddress = async (client, propertyId, address) => {
  const query = {
    name: 'update-address',
    // language=SQL
    text: `
          update addresses set
            line1 = $1,
            line2 = $2,
            line3 = $3,
            line4 = $4,
            post_code = $5,
            city = $6,
            country = $7
          where id = (select address_id from properties p where p.id = $8)
          returning *
        `,
  };

  const result = await client.query({
    ...query,
    values: [
      address.line1,
      address.line2,
      address.line3,
      address.line4,
      address.postCode,
      address.city,
      address.country,
      propertyId,
    ],
  });

  return result.rows[0];
};

const updateProperty = async (client, id, property) => {
  const query = {
    name: 'update-property',
    // language=SQL
    text: `
          update properties set
            owner = $1,
            number_of_bedrooms = $2,
            number_of_bathrooms = $3,
            income_generated = $4,
            airbnb_id = $5
          where id = $6
          returning *
        `,
  };

  const addressRow = await updateAddress(client, id, property.address);

  const result = await client.query({
    ...query,
    values: [
      property.owner,
      property.numberOfBedrooms,
      property.numberOfBathrooms,
      property.incomeGenerated,
      property.airbnbId,
      id,
    ],
  });

  return mapProperties(result.rows[0], addressRow);
};

const fetchProperty = async (client, id) => {
  const query = {
    name: 'fetch-property',
    // language=SQL
    text: `
          select
            p.id,
            p.owner,
            p.address_id,
            p.number_of_bedrooms,
            p.number_of_bathrooms,
            p.income_generated,
            p.airbnb_id,
            a.line1,
            a.line2,
            a.line3,
            a.line4,
            a.post_code,
            a.city,
            a.country
          from properties p
            join addresses a on a.id = p.address_id
          where p.id = $1
        `,
  };

  const result = await client.query({ ...query, values: [id] });

  if (result.rows.length === 0) {
    return null;
  }

  return mapProperties(result.rows[0]);
};

const fetchProperties = async (client) => {
  const query = {
    name: 'fetch-properties',
    // language=SQL
    text: `
          select
            p.id,
            p.owner,
            p.address_id,
            p.number_of_bedrooms,
            p.number_of_bathrooms,
            p.income_generated,
            p.airbnb_id,
            a.line1,
            a.line2,
            a.line3,
            a.line4,
            a.post_code,
            a.city,
            a.country
          from properties p
            join addresses a on a.id = p.address_id
        `,
  };

  const result = await client.query({ ...query });

  return result.rows.map(row => mapProperties(row));
};

module.exports = {
  fetchProperties,
  fetchProperty,
  createProperty,
  updateProperty,
};
