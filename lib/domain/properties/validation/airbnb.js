const Repo = require('./airbnb-id.repo');

const requestIdValidation = (client, airbnbId) => {
  Repo.saveValidationRequest(client, airbnbId);
};

const resolveStatuses = (client, airbnbIds) => Repo.resolveValidationStatuses(client, airbnbIds);

const getNextUnresolvedId = client => Repo.getNextUnresolvedId(client);

const setValidationStatus = async (client, airbnbId, status) => {
  await Repo.saveResponseStatus(client, airbnbId, status);
};

module.exports = {
  requestIdValidation,
  resolveStatuses,
  getNextUnresolvedId,
  setValidationStatus,
};
