const getRequestId = async (client, airbnbId) => {
  const query = {
    name: 'get-validation-request-id',
    // language=SQL
    text: `
      select id
      from airbnb_validation_requests
      where airbnb_id = $1
    `,
  };

  const result = await client.query({ ...query, values: [airbnbId] });
  if (!result.rows.length) {
    return null;
  }

  return result.rows[0].id;
};

const saveValidationRequest = async (client, airbnbId) => {
  const query = {
    name: 'create-validation-request',
    // language=SQL
    text: 'insert into airbnb_validation_requests (airbnb_id) values ($1)',
  };

  const existingRequestId = await getRequestId(client, airbnbId);
  if (existingRequestId != null) {
    return;
  }

  await client.query({ ...query, values: [airbnbId] });
};

const saveResponseStatus = async (client, airbnbId, status) => {
  const query = {
    name: 'create-validation-response',
    // language=SQL
    text: 'insert into airbnb_validation_results (request_id, result) values ($1, $2)',
  };

  const requestId = await getRequestId(client, airbnbId);
  if (requestId === null) {
    return;
  }

  await client.query({ ...query, values: [requestId, status] });
};

const resolveValidationStatuses = async (client, airbnbIds) => {
  const query = {
    name: 'resolve-validation-statuses',
    // language=SQL
    text: `
      with last_results as (
        select request_id, max(created_on) as created_on
        from airbnb_validation_results
        group by request_id
      )

      select req.airbnb_id, coalesce(res.result, 'PENDING') as result
      from airbnb_validation_requests req
        left join last_results last_res on last_res.request_id = req.id
        left join airbnb_validation_results res on res.request_id = req.id and res.created_on = last_res.created_on
      where req.airbnb_id = any ($1::text[])
    `,
  };

  const result = await client.query({ ...query, values: [airbnbIds] });

  const statuses = {};
  result.rows.forEach((row) => {
    statuses[row.airbnb_id] = row.result;
  });

  return statuses;
};

const getNextUnresolvedId = async (client) => {
  const query = {
    name: 'next-unresolved-airbnb-statuses',
    // language=SQL
    text: `
      select req.airbnb_id
      from airbnb_validation_requests req
        left join last_airbnb_validation_results res on res.request_id = req.id
      where res.result is null or res.result not in ('VALID', 'INVALID')
      order by res.created_on nulls first
      limit 1
    `,
  };

  const result = await client.query(query);
  if (!result.rows.length) {
    return null;
  }

  return result.rows[0].airbnb_id;
};

module.exports = {
  saveValidationRequest,
  saveResponseStatus,
  resolveValidationStatuses,
  getNextUnresolvedId,
};
