class ApplicationError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

class ValidationError extends ApplicationError {
  constructor(message, error) {
    super(message);
    this.status = 400;
    this.error = error;
  }

  get body() {
    return {
      message: this.message,
      body: this.error,
    };
  }
}

class NotFoundError extends ApplicationError {
  constructor(resource, id) {
    super(`${resource} with id ${id} was not found`);
    this.status = 404;
  }
}

module.exports = {
  ApplicationError,
  ValidationError,
  NotFoundError,
};
