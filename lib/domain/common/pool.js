const { Pool } = require('pg');
const config = require('../../config');

const pool = new Pool({
  user: config.db.username,
  password: config.db.password,
  database: config.db.database,
  host: config.db.host,
  port: config.db.port,
  poolSize: config.db.poolSize,
});

const executeInTransaction = async (callback) => {
  const client = await pool.connect();
  let result;

  try {
    await client.query('begin');

    result = await callback(client);

    await client.query('commit');
  } catch (err) {
    await client.query('rollback');
    throw err;
  } finally {
    client.release();
  }

  return result;
};

module.exports = executeInTransaction;
