const axios = require('axios');

const api = axios.create({
  baseURL: 'https://www.airbnb.co.uk',
  maxRedirects: 0,
});

const isIdValid = async (airbnbId) => {
  const { status } = await api.get(`/rooms/${airbnbId}`);
  if (status >= 200 && status < 300) {
    return true;
  }

  if (status >= 300 && status < 400) {
    return false;
  }

  throw new Error(`Could not determine if provided airbnb id is valid: ${airbnbId}`);
};

module.exports = {
  isIdValid,
};
