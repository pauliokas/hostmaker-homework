const winston = require('winston');

const {
  combine, timestamp, colorize, printf,
} = winston.format;

const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
  format: combine(
    colorize(),
    timestamp(),
    printf(info => `${info.timestamp} ${info.level} \t- ${info.message}`),
  ),
});

module.exports = logger;
