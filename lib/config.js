module.exports = {
  port: process.env.HOSTMAKERS_PORT || 3000,
  db: {
    username: process.env.PGUSER || 'postgres',
    password: process.env.PGPASSWORD || 'postgres',
    database: process.env.PGDATABASE || 'postgres',
    host: process.env.PGHOST || 'localhost',
    port: process.env.PGPORT || 5432,
    poolSize: process.env.DB_POOL_SIZE || 10,
  },
};
