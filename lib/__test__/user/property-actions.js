const { merge } = require('lodash');
const faker = require('faker'); /* eslint import/no-extraneous-dependencies: 0 */
const api = require('./api');

const getMerged = patch => merge({
  owner: faker.name.findName(),
  numberOfBedrooms: faker.random.number(3) + 1,
  numberOfBathrooms: faker.random.number(3) + 1,
  incomeGenerated: faker.random.number() + 1,
  airbnbId: '2354700',
  address: {
    line1: faker.address.streetAddress(),
    line2: faker.address.secondaryAddress(),
    line3: faker.address.secondaryAddress(),
    line4: faker.address.county(),
    postCode: faker.address.zipCode(),
    city: faker.address.city(),
    country: faker.address.country(),
  },
}, patch);

const createProperty = async (patch = {}) => api.post('/properties', getMerged(patch));

const updateProperty = async (id, patch = {}) => api.put(`/properties/${id}`, getMerged(patch));

const patchProperty = async (id, patch = {}) => api.patch(`/properties/${id}`, patch);

const getProperty = async id => api.get(`/properties/${id}`);

const getProperties = async () => api.get('/properties');

module.exports = {
  createProperty,
  updateProperty,
  patchProperty,
  getProperty,
  getProperties,
};
