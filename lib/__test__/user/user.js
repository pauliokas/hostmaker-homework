const axios = require('axios');
const _ = require('lodash');
const faker = require('faker'); /* eslint import/no-extraneous-dependencies: "off" */

const api = axios.create({
  baseURL: 'http://localhost:3000',
});

const defaultProperty = () => ({
  owner: faker.name.findName(),
  numberOfBedrooms: faker.random.number(3) + 1,
  numberOfBathrooms: faker.random.number(3) + 1,
  incomeGenerated: faker.random.number() + 1,
  airbnbId: '2354700',
  address: {
    line1: faker.address.streetAddress(),
    line2: faker.address.secondaryAddress(),
    line3: faker.address.secondaryAddress(),
    line4: faker.address.county(),
    postCode: faker.address.zipCode(),
    city: faker.address.city(),
    country: faker.address.country(),
  },
});

const doRequest = async (req) => {
  try {
    return await req();
  } catch (err) {
    return err.response;
  }
};

const createProperty = async (stub = {}) => {
  const property = _.merge(defaultProperty(), stub);
  return doRequest(() => api.post('/properties', property));
};

const updateProperty = async (id, stub = {}) => {
  const property = _.merge(defaultProperty(), stub);
  return doRequest(() => api.put(`/properties/${id}`, property));
};

const getProperty = async id => doRequest(() => api.get(`/properties/${id}`));

const getProperties = async () => doRequest(() => api.get('/properties'));

module.exports = {
  createProperty,
  updateProperty,
  getProperty,
  getProperties,
};
