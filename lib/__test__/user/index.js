const PropertyActions = require('./property-actions');
const ChangesActions = require('./changes-actions');

module.exports = {
  ...PropertyActions,
  ...ChangesActions,
};
