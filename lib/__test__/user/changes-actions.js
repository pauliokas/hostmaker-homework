const api = require('./api');

const fetchChanges = filter => api.get('/changes', { params: filter });

module.exports = {
  fetchChanges,
};
