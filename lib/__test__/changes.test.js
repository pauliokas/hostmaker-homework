const { mapValues } = require('lodash');
const User = require('./user');

describe('/changes', () => {
  describe('finds by property', () => {
    let firstProperty;
    let secondProperty;
    beforeEach(async () => {
      firstProperty = (await User.createProperty()).data;
      secondProperty = (await User.createProperty()).data;
      await User.updateProperty(firstProperty.id, { owner: firstProperty.owner });
    });

    [
      {
        filter: { actions: () => ['CHANGED'], propertyIds: () => [firstProperty.id, secondProperty.id] },
        changes: 1,
      },
      {
        filter: { paths: () => ['owner'], propertyIds: () => [firstProperty.id, secondProperty.id] },
        changes: 2,
      },
      {
        filter: { propertyIds: () => [firstProperty.id] },
        changes: 2,
      },
    ].map(template => test(`filters by '${Object.keys(template.filter)}'`, async () => {
      const result = await User.fetchChanges(mapValues(template.filter, val => val()));

      expect(result.status).toEqual(200);
      expect(result.data.length).toEqual(template.changes);
    }));
  });
});
