const User = require('./user');

describe('/properties', () => {
  test('creates property', async () => {
    const result = await User.createProperty({
      owner: 'Owner',
      numberOfBedrooms: 1,
      numberOfBathrooms: 2,
      incomeGenerated: '3.5243',
      airbnbId: '2354700',
      address: {
        line1: 'Line 1',
        line2: 'Line 2',
        line3: 'Line 3',
        line4: 'Line 4',
        postCode: 'Postal Code',
        city: 'City',
        country: 'Country',
      },
    });

    expect(result.status).toEqual(201);
    expect(result.headers).toHaveProperty('location');
    expect(result.data).toHaveProperty('id');
    expect(result.data).toHaveProperty('owner', 'Owner');
    expect(result.data).toHaveProperty('numberOfBedrooms', 1);
    expect(result.data).toHaveProperty('numberOfBathrooms', 2);
    expect(result.data).toHaveProperty('incomeGenerated', '3.5243');
    expect(result.data).toHaveProperty('airbnbId', '2354700');
    expect(result.data).toHaveProperty('address', {
      line1: 'Line 1',
      line2: 'Line 2',
      line3: 'Line 3',
      line4: 'Line 4',
      postCode: 'Postal Code',
      city: 'City',
      country: 'Country',
    });
  });

  test('updates property', async () => {
    const createResult = await User.createProperty();
    const propertyId = createResult.data.id;

    const updateResult = await User.updateProperty(propertyId, {
      owner: 'Owner',
      numberOfBedrooms: 1,
      numberOfBathrooms: 2,
      incomeGenerated: '3.5243',
      airbnbId: '2354700',
      address: {
        line1: 'Line 1',
        line2: 'Line 2',
        line3: 'Line 3',
        line4: 'Line 4',
        postCode: 'Postal Code',
        city: 'City',
        country: 'Country',
      },
    });

    expect(updateResult.status).toEqual(200);
    expect(updateResult.data).toHaveProperty('id', propertyId);
    expect(updateResult.data).toHaveProperty('owner', 'Owner');
    expect(updateResult.data).toHaveProperty('numberOfBedrooms', 1);
    expect(updateResult.data).toHaveProperty('numberOfBathrooms', 2);
    expect(updateResult.data).toHaveProperty('incomeGenerated', '3.5243');
    expect(updateResult.data).toHaveProperty('airbnbId', '2354700');
    expect(updateResult.data).toHaveProperty('address', {
      line1: 'Line 1',
      line2: 'Line 2',
      line3: 'Line 3',
      line4: 'Line 4',
      postCode: 'Postal Code',
      city: 'City',
      country: 'Country',
    });
  });

  test('fetches property', async () => {
    const createResult = await User.createProperty();
    const propertyId = createResult.data.id;

    const updateResult = await User.getProperty(propertyId);

    expect(updateResult.status).toEqual(200);
    expect(updateResult.data).toEqual(createResult.data);
  });

  test('fetches properties', async () => {
    await Promise.all(Array(3).map(() => User.createProperty()));

    const result = await User.getProperties();

    expect(result.status).toEqual(200);
    expect(result.data.length).toBeGreaterThanOrEqual(3);
  });

  test('patches property', async () => {
    const createResult = await User.createProperty();
    const propertyId = createResult.data.id;
    const patchResult = await User.patchProperty(propertyId, { owner: 'Another owner' });

    expect(patchResult.status).toEqual(200);
    expect(patchResult.data).toHaveProperty('owner', 'Another owner');
  });
});
