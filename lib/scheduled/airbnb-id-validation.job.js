const executeInTransaction = require('../domain/common/pool');
const {
  getNextUnresolvedId,
  setValidationStatus,
} = require('../domain/properties/validation/airbnb');
const airbnb = require('../domain/airbnb/api');

const resolveNext = () => executeInTransaction(async (client) => {
  const airbnbId = await getNextUnresolvedId(client);
  if (airbnbId === null) {
    return null;
  }

  let status;
  try {
    status = (await airbnb.isIdValid(airbnbId)) ? 'VALID' : 'INVALID';
  } catch (err) {
    status = 'ERROR';
  }

  await setValidationStatus(client, airbnbId, status);

  return status;
});

const start = (interval) => {
  const intervalHandle = setInterval(async () => {
    const status = await resolveNext();
    if (status === 'ERROR') {
      clearInterval(intervalHandle);
      setTimeout(() => start(interval), 10000);
    }
  }, interval);
};

module.exports = {
  start,
};
