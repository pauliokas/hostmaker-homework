FROM node:carbon-alpine

EXPOSE 3000

RUN apk add --no-cache tini
ENTRYPOINT [ "tini", "--" ]
CMD ["node", "index.js"]

ENV NODE_ENV production

WORKDIR /app

COPY package*.json ./
RUN npm install --only=production

COPY . .

USER node
